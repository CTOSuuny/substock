from django.apps import AppConfig


class WithdrawListConfig(AppConfig):
    name = 'Withdraw_list'
