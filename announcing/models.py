import os
from django.db import models
from binascii import hexlify

class list(models.Model):

	def _createId():
		pool = string.ascii_letters + string.digits
		return ''.join(random.choice(pool) for i in range(64))

	id = models.AutoField(primary_key=True)
	announc_list_id = models.CharField(max_length=30,default=_createId,db_index=True)
	announc_subject = models.CharField(max_length=100)
	announc_detail = models.CharField(max_length=100)
	announc_by = models.ForeignKey('account.Account',on_delete=models.CASCADE,null=True)
	announc_role = models.ForeignKey('account.RoleAccount',on_delete=models.CASCADE,null=True)
	date_create = models.DateTimeField(auto_now_add=True)
	date_update = models.DateTimeField(auto_now=True)

	def __str__(self):
		return self.announc_subject
