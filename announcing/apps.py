from django.apps import AppConfig


class AnnouncingConfig(AppConfig):
    name = 'announcing'
