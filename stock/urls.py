from django.urls import path
from django.conf.urls import url, include
from django.contrib import admin
from rest_framework.routers import DefaultRouter
from stock.views import StockList,StockDetailList,StockCreateList,roleStockList,roleStockDetailList,roleStockCreateList,ItemList,ItemDetailList,ItemCreateList
# schema_view = get_schema_view(title='Pastebin API') # new

urlpatterns = [
	path('', StockList.as_view(), name="stock-list"),
	path('create/', StockCreateList.as_view(), name="stock-create"),
	path('<int:pk>/', StockDetailList.as_view(), name="stock-detail"),

	path('role/', roleStockList.as_view(), name="rolestock-list"),
	path('role/create/', roleStockCreateList.as_view(), name="rolestock-create"),
	path('role/<int:pk>/', roleStockDetailList.as_view(), name="rolestock-detail"),

	path('item/', ItemList.as_view(), name="item-list"),
	path('item/create/', ItemCreateList.as_view(), name="item-create"),
	path('item/<int:pk>/', ItemDetailList.as_view(), name="item-detail"),
]

# urlpatterns = format_suffix_patterns(urlpatterns)