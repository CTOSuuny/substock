from rest_framework import serializers
from .models import Stock,RoleStock,Item
from account.models import RoleAccount,Account


class stockSerializer(serializers.ModelSerializer):

    class Meta:
        model = Stock
        fields = ('url','id', 'stock_name', 'stock_id', 'stock_detail' )

class stockDetailSerializer(serializers.ModelSerializer):

    class Meta:
        model = Stock
        fields = "__all__"

class stockCreateSerializer(serializers.HyperlinkedModelSerializer): # new
	stock_own = serializers.CharField(write_only=True)

	class Meta:
		model = Stock
		fields = ( 'id', 'stock_name','stock_detail','stock_own')

	def create(self, validated_data):
		validated_data['stock_own'] = Account.objects.get(id=validated_data['stock_own'])
		user = super(stockCreateSerializer, self).create(validated_data)
		user.save()
		return user

#################################################################################################

class rolestockSerializer(serializers.ModelSerializer):

	class Meta:
		model = RoleStock
		depth = 1
		fields = ('url', 'id', 'role_stock', 'role_stock_account' )

class rolestockDetailSerializer(serializers.ModelSerializer):

    class Meta:
        model = RoleStock
        fields = "__all__"

class rolestockCreateSerializer(serializers.ModelSerializer): # new
	role_stock = serializers.CharField(write_only=True)
	role_stock_account = serializers.CharField(write_only=True)

	class Meta:
		model = RoleStock
		fields = ( 'role_stock', 'role_stock_account' )

	def create(self, validated_data):
		validated_data['role_stock'] = Stock.objects.get(id=validated_data['role_stock'])
		validated_data['role_stock_account'] = RoleAccount.objects.get(id=validated_data['role_stock_account'])
		role = super(rolestockCreateSerializer, self).create(validated_data)
		role.save()
		return role

#################################################################################################

class ItemSerializer(serializers.ModelSerializer):

	class Meta:
		model = Item
		depth = 1
		fields = ('url', 'id', 'item_name','item_detail','item_unit','item_sub_unit','item_serial_code',
)

class ItemDetailSerializer(serializers.ModelSerializer):

    class Meta:
        model = Item
        fields = "__all__"

class ItemCreateSerializer(serializers.HyperlinkedModelSerializer): # new
	stock_f = serializers.CharField(write_only=True)

	class Meta:
		model = Item
		fields = ( 'id', 'item_name','item_detail','item_unit','item_sub_unit','item_serial_code','stock_f')

	def create(self, validated_data):
		validated_data['stock_f'] = Stock.objects.get(id=validated_data['stock_f'])
		item = super(ItemCreateSerializer, self).create(validated_data)
		item.save()
		return item