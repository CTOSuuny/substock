import os
import os,binascii
import random
import string
from django.db import models
from binascii import hexlify

class Stock(models.Model):

	def _createId():
		pool = string.ascii_letters + string.digits
		return ''.join(random.choice(pool) for i in range(64))

	id = models.AutoField(primary_key=True)
	stock_name = models.CharField(max_length=30,db_index=True)
	stock_id = models.CharField(db_index=True,max_length=100,default=_createId,unique=True)
	stock_detail = models.CharField(max_length=100)
	stock_own = models.ForeignKey('account.Account',on_delete=models.CASCADE,null=True)
	date_create = models.DateTimeField(auto_now_add=True)
	date_update = models.DateTimeField(auto_now=True)

	def __str__(self):
		return self.stock_name

	def as_dict(self):

		items = Item.objects.filter(stock_f=self.id)
		item = [ obj.as_dict_item() for obj in items ]

		return {
			"id": self.id,
			"stock_name" : self.stock_name,
			"stock_id" : self.stock_id,
			"stock_detail" : self.stock_detail,
			"stock_own" : self.stock_own.as_dict(),
			"items" : item,
			"date_create" : self.date_create,
			"date_update" : self.date_update,
		}

	def as_dict_stock(self,current_site):

		return {
			"url" : current_site+'/stock/'+str(self.id)+'/',
			"id": self.id,
			"stock_name" : self.stock_name,
			"stock_id" : self.stock_id,
			"stock_detail" : self.stock_detail,
			"stock_own" : self.stock_own.as_dict(),
			"date_create" : self.date_create,
			"date_update" : self.date_update,
		}

class RoleStock(models.Model):

	def _createId():
		pool = string.ascii_letters + string.digits
		return ''.join(random.choice(pool) for i in range(64))

	id = models.AutoField(primary_key=True)
	# role_stock_name = models.CharField(max_length=30,db_index=True)
	role_stock = models.ForeignKey('Stock',related_name='roles',on_delete=models.CASCADE,null=True)
	role_stock_account = models.ForeignKey('account.RoleAccount',on_delete=models.CASCADE,null=True)
	# role_stock_detail = models.CharField(max_length=100)
	date_create = models.DateTimeField(auto_now_add=True)
	date_update = models.DateTimeField(auto_now=True)

	def as_dict(self):
		return {
			"id": self.id,
			"role_stock" : self.role_stock.as_dict_stock(),
			"role_stock_account" : self.role_stock_account.as_dict(),
		}

class RoleStockOwn(models.Model):

	def _createId():
		pool = string.ascii_letters + string.digits
		return ''.join(random.choice(pool) for i in range(64))

	id = models.AutoField(primary_key=True)
	# role_stock_name = models.CharField(max_length=30,db_index=True)
	own_stock = models.ForeignKey('Stock',related_name='owns',on_delete=models.CASCADE,null=True)
	own_stock_account = models.ForeignKey('account.RoleAccount',on_delete=models.CASCADE,null=True)
	# role_stock_detail = models.CharField(max_length=100)
	date_create = models.DateTimeField(auto_now_add=True)
	date_update = models.DateTimeField(auto_now=True)

	def as_dict(self):
		return {
			"id": self.id,
			"role_stock" : self.own_stock.as_dict_stock(),
			"role_stock_account" : self.own_stock_account.as_dict(),
		}

class Item(models.Model):

	def _createId():
		pool = string.ascii_letters + string.digits
		return ''.join(random.choice(pool) for i in range(64))

	id = models.AutoField(primary_key=True)
	item_name = models.CharField(max_length=30,db_index=True)
	item_id = models.CharField(db_index=True,max_length=100,default=_createId,unique=True)
	item_detail = models.CharField(max_length=100)
	item_unit = models.CharField(max_length=100,null=True)
	item_sub_unit = models.CharField(max_length=100,null=True)
	item_serial_code = models.CharField(max_length=100,db_index=True,null=True)
	stock_f = models.ForeignKey('Stock',on_delete=models.CASCADE,null=True)
	date_create = models.DateTimeField(auto_now_add=True)
	date_update = models.DateTimeField(auto_now=True)

	def __str__(self):
		return self.item_name

	def as_dict(self):
		return {
			"id": self.id,
			"item_name" : self.item_name,
			"item_id" : self.item_id,
			"item_detail" : self.item_detail,
			"item_unit" : self.item_unit,
			"item_sub_unit" : self.item_sub_unit,
			"item_serial_code" : self.item_serial_code,
			"stock" : self.stock_f.as_dict(),
		}

	def as_dict_item(self):
		return {
			"id": self.id,
			"item_name" : self.item_name,
			"item_id" : self.item_id,
			"item_detail" : self.item_detail,
			"item_unit" : self.item_unit,
			"item_sub_unit" : self.item_sub_unit,
			"item_serial_code" : self.item_serial_code,
		}