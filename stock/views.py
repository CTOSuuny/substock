from rest_framework import generics, permissions, renderers
from django.core import serializers
from .models import Stock,RoleStock,Item
from account.models import Account,RoleAccount
from rest_framework.authtoken.models import Token
from django.http import JsonResponse
from .serializers import stockSerializer,stockDetailSerializer,stockCreateSerializer,rolestockSerializer,rolestockDetailSerializer,rolestockCreateSerializer,ItemSerializer,ItemDetailSerializer,ItemCreateSerializer
from rest_framework import status
from rest_framework.status import (
    HTTP_400_BAD_REQUEST,
    HTTP_404_NOT_FOUND,
    HTTP_200_OK,
)

class StockList(generics.ListCreateAPIView):
	# queryset = Stock.objects.all()
	# serializer_class = stockSerializer
	permission_classes = (permissions.IsAuthenticated,)

	def get(self, request, format=None):
		current_site = request.META['HTTP_HOST']
		print((request.is_secure() and "https" or "http")+'://'+ current_site)
		
		my_token = request.META.get('HTTP_AUTHORIZATION').split()[1]
		token = Token.objects.get(key=my_token)
		user = Account.objects.get(id=token.user_id)
		role_stock = RoleStock.objects.filter(role_stock_account=user.role_ID)

		stock = [ obj.as_dict_stock((request.is_secure() and "https" or "http")+'://'+ current_site) for obj in [ obj.role_stock for obj in role_stock ] ]

		return JsonResponse({"result":stock},status=status.HTTP_200_OK)
		# return JsonResponse({"result":"sample"},status=status.HTTP_200_OK)

class StockDetailList(generics.RetrieveUpdateDestroyAPIView):
	# queryset = Stock.objects.all()
	# serializer_class = stockDetailSerializer
	permission_classes = (permissions.IsAuthenticated,)

	def get(self, request, pk):
		stock = Stock.objects.get(id=pk)
		items = Item.objects.filter(stock_f=pk)
		item = [ obj.as_dict() for obj in items ]

		return JsonResponse({"result":stock.as_dict()},status=status.HTTP_200_OK)

class StockCreateList(generics.CreateAPIView):
    queryset = Stock.objects.all()
    serializer_class = stockCreateSerializer
    permission_classes = (permissions.IsAuthenticated,)

########################################################################################
    ##########################        Role           ###############################
########################################################################################

class roleStockList(generics.ListCreateAPIView):
	queryset = RoleStock.objects.all()
	serializer_class = rolestockSerializer
	permission_classes = (permissions.IsAuthenticated,)

class roleStockDetailList(generics.RetrieveUpdateDestroyAPIView):
	# queryset = RoleStock.objects.all()
	serializer_class = rolestockDetailSerializer
	permission_classes = (permissions.IsAuthenticated,)

	def get(self, request, pk):
		role_stock = RoleStock.objects.get(id=pk)
		return JsonResponse({"result":role_stock.as_dict()},status=status.HTTP_200_OK)
	    

class roleStockCreateList(generics.CreateAPIView):
    queryset = RoleStock.objects.all()
    serializer_class = rolestockCreateSerializer
    permission_classes = (permissions.IsAuthenticated,)

########################################################################################
    ##########################        Item           ###############################
########################################################################################

class ItemList(generics.ListCreateAPIView):
	queryset = Item.objects.all()
	serializer_class = ItemSerializer
	permission_classes = (permissions.IsAuthenticated,)

class ItemDetailList(generics.RetrieveUpdateDestroyAPIView):
	# queryset = RoleStock.objects.all()
	serializer_class = ItemDetailSerializer
	permission_classes = (permissions.IsAuthenticated,)

	def get(self, request, pk):
		role_stock = Item.objects.get(id=pk)
		return JsonResponse({"result":role_stock.as_dict()},status=status.HTTP_200_OK)
	    

class ItemCreateList(generics.CreateAPIView):
    queryset = Item.objects.all()
    serializer_class = ItemCreateSerializer
    permission_classes = (permissions.IsAuthenticated,)