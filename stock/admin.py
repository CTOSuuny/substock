from django.contrib import admin
from . models import Stock, RoleStock, Item, RoleStockOwn
# Register your models here.
admin.site.register(Stock)
admin.site.register(RoleStock)
admin.site.register(RoleStockOwn)
admin.site.register(Item)
