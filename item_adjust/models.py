import os
from django.db import models
from binascii import hexlify

class item_adjust_list(models.Model):

	def _createId():
		pool = string.ascii_letters + string.digits
		return ''.join(random.choice(pool) for i in range(64))

	id = models.AutoField(primary_key=True)
	item_list_id = models.CharField(max_length=30,default=_createId,db_index=True)
	item_document_f = models.ForeignKey('item_adjust_document', on_delete=models.CASCADE)
	item_note = models.CharField(max_length=100)
	item_f = models.ForeignKey('stock.Item', on_delete=models.CASCADE)
	date_create = models.DateTimeField(auto_now_add=True)
	date_update = models.DateTimeField(auto_now=True)

	def __str__(self):
		return self.name

class item_adjust_document(models.Model):

	def _createId():
		pool = string.ascii_letters + string.digits
		return ''.join(random.choice(pool) for i in range(64))

	id = models.AutoField(primary_key=True)
	item_document_id = models.CharField(db_index=True,max_length=100,default=_createId,unique=True)
	item_note = models.CharField(max_length=100)
	item_own = models.ForeignKey('account.Account',on_delete=models.CASCADE,null=True)
	date_create = models.DateTimeField(auto_now_add=True)
	date_update = models.DateTimeField(auto_now=True)

	def __str__(self):
		return self.name