from django.apps import AppConfig


class ItemAdjustConfig(AppConfig):
    name = 'item_adjust'
