from django.conf import settings
from django.http import HttpResponse, HttpResponseBadRequest, HttpResponseForbidden
from django.views.decorators.csrf import csrf_exempt
from django.utils.html import escape
import json
import os.path
from pyzbar.pyzbar import decode
import cv2,requests,os



@csrf_exempt
def callback(request):
    if request.method == 'POST':
        # signature = request.META['HTTP_X_LINE_SIGNATURE']

        header_key = "ZUxkwcNaapXmpV/gacIdPIRERyV/CPLTb6VlSj75snAXIpwmCh3eHlYnjMnhfQ4DDTDMbHZBdUTl9CWLQPCbd9NRrfK7kdBEGO6f2NkETTjjl7NHg2h+uzXF7RlSNiGM+BrqFQmWAfPRXwN7t4c8GgdB04t89/1O/w1cDnyilFU="

        path = '/home/dev/substock/line_app/log/'
        # path = '/home/sunnypdater/project/substock/line_app/log/'
        completeLog = os.path.join(path, "log.txt")  
        completeAccess = os.path.join(path, "access.txt")  
        
        headers = {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer {'+header_key+'}',
        }


        ###################  processing  #################

        body_unicode = request.body.decode('utf-8')
        if body_unicode:
        
            body = json.loads(body_unicode)

            f= open(completeLog,"a+")
            f.write(json.dumps(body)+ "\n\n\n")
            f.close() 

            typer = body["events"][0]["message"]["type"]

            data = {}

            if typer == "text":
                message = body["events"][0]["message"]["text"]
                if message == "สวัสดี":
                    data["replyToken"] = body["events"][0]["replyToken"];

                    mes = {}
                    mes["type"] = "text";
                    mes["text"] = "สวัสดีคร้าบบบบบบ";

                    data["messages"] = [];
                    data["messages"].append(mes);
                    cdata = json.dumps(data)
                    res = replyMsg(headers,cdata);
                    str1 = res+"\r\n"
                    f= open(completeAccess,"a+")
                    f.write(str1)
                    f.close() 

                else:
                    data["replyToken"] = body["events"][0]["replyToken"];

                    mes = {}
                    mes["type"] = "text";
                    mes["text"] = "อ่านไม่ออก";

                    data["messages"] = [];
                    data["messages"].append(mes);
                    cdata = json.dumps(data)

                    res = replyMsg(headers,cdata);
                    str1 = res+"\r\n"

                    f= open(completeAccess,"a+")
                    f.write(str1)
                    f.close() 

            elif typer == "image":

                idm = body["events"][0]["message"]["id"];
                # data["replyToken"] = body["events"][0]["replyToken"];
                userid = body["events"][0]["source"]["userId"];

                response = requests.get('https://api.line.me/v2/bot/message/'+idm+'/content', headers={'Authorization': 'Bearer '+header_key})
                image=open('/home/dev/substock/line_app/tmpbc/'+idm+'.jpg', 'wb').write(response.content)
                objs = decode(cv2.imread('/home/dev/substock/line_app/tmpbc/'+idm+'.jpg'))
                for obj in objs:
                    # print('type : ',obj.type ,' data : ',(obj.data).decode("utf-8") ,'\n')

                    mes = {}
                    mes["type"] = "text";
                    # mes["text"] = "สวัสดีคร้าบบบบบบ";
                    mes["text"] = 'type : '+obj.type +' data : '+(obj.data).decode("utf-8");

                    data["messages"] = [];
                    data["messages"].append(mes);

                    data["to"] = body["events"][0]["source"]["userId"];
                    cdata = json.dumps(data)
                    res = postMsg(headers,cdata);
                    
                os.remove("/home/dev/substock/line_app/tmpbc/"+idm+".jpg")


        return HttpResponse()
    if request.method == 'GET':
        return HttpResponse({'success'})
        pass
    else:
        return HttpResponseBadRequest()

def replyMsg(header,data):
        response = requests.post("https://api.line.me/v2/bot/message/reply", headers=header, data=data)
        return str(response)

def postMsg(header,data):
        response = requests.post("https://api.line.me/v2/bot/message/push", headers=header, data=data)
        return str(response)
