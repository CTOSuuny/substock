from django.urls import path
from django.conf.urls import url, include
from django.contrib import admin
from rest_framework.routers import DefaultRouter
from account.views import userList, userDetailList, UserCreateAPIView, Logout, RoleUserList, RoleUserDetailList, RoleUserCreateList, otp_generate

# schema_view = get_schema_view(title='Pastebin API') # new

urlpatterns = [
	path('otp_gen/', otp_generate.as_view()),

	path('', userList.as_view(), name="account-list"),
	path('role/', RoleUserList.as_view(), name="role-list"),
	path('<int:pk>/', userDetailList.as_view(), name="account-detail"),
	path('role/<int:pk>/', RoleUserDetailList.as_view(), name="roleaccount-detail"),
	
	path('create/', UserCreateAPIView.as_view(), name="account-create"),
	path('role/create/', RoleUserCreateList.as_view(), name="roleaccount-create"),
]

# urlpatterns = format_suffix_patterns(urlpatterns)