from django.conf import settings
from rest_framework import generics, permissions, renderers # new
from .models import Account, RoleAccount
from .serializers import userSerializer, userDetailSerializer, userCreateSerializer, RoleuserSerializer, RoleuserDetailSerializer, RoleuserCreateSerializer, UserSigninSerializer, usersignedSerializer
from .authentication import token_expire_handler, expires_in
from rest_framework.response import Response
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.authtoken.models import Token
from rest_framework.response import Response
from rest_framework import status

from django.contrib.auth import authenticate
from django.http import JsonResponse
import math, random 
from rest_framework.status import (
    HTTP_400_BAD_REQUEST,
    HTTP_404_NOT_FOUND,
    HTTP_200_OK,
)

# @api_view(['GET','POST']) # new
# def api_root(request, format=None):

#     return Response({
#         'users': reverse('user-list', request=request, format=format),
#         'api': reverse('api-list', request=request, format=format)
#     })

class RoleUserList(generics.ListCreateAPIView):
    queryset = RoleAccount.objects.all()
    serializer_class = RoleuserSerializer
    permission_classes = (permissions.IsAuthenticated,)

class RoleUserDetailList(generics.RetrieveUpdateDestroyAPIView):
    queryset = RoleAccount.objects.all()
    serializer_class = RoleuserDetailSerializer
    permission_classes = (permissions.IsAuthenticated,)

class RoleUserCreateList(generics.ListCreateAPIView):
    queryset = RoleAccount.objects.all()
    serializer_class = RoleuserCreateSerializer
    permission_classes = (permissions.IsAuthenticated,)


##########################################################################
##########################################################################
##########################################################################

class userList(generics.ListCreateAPIView):
    queryset = Account.objects.all()
    serializer_class = userSerializer
    permission_classes = (permissions.IsAuthenticated,)

class userDetailList(generics.RetrieveUpdateDestroyAPIView):
    queryset = Account.objects.all()
    serializer_class = userDetailSerializer
    permission_classes = (permissions.IsAuthenticated,)

class UserCreateAPIView(generics.CreateAPIView):
	queryset = Account.objects.all()
	serializer_class = userCreateSerializer
	permission_classes = (permissions.IsAuthenticated,)

class LoginAuthToken(ObtainAuthToken):
	def post(self, request, *args, **kwargs):
		# response = super(LoginAuthToken, self).post(request, *args, **kwargs)
		# token = Token.objects.get(key=response.data['token'])
		# return Response({'token': token.key, 'id': token.user_id})

		signin_serializer = UserSigninSerializer(data = request.data)
		if not signin_serializer.is_valid():
			return Response(signin_serializer.errors, status = HTTP_400_BAD_REQUEST)

						

		user = authenticate(
		        username = signin_serializer.data['username'],
		        password = signin_serializer.data['password'] 
		    )
		if not user:
			return Response({'detail': 'Invalid Credentials or activate account'}, status=HTTP_404_NOT_FOUND)
		    
		#TOKEN STUFF
		token, _ = Token.objects.get_or_create(user = user)

		is_remember = request.POST.get('remember', False)
		
		#REMEBER
		if is_remember == True or is_remember == '1':

			user_id = Token.objects.get(key=token).user_id
			user = Account.objects.get(id=user_id)
			user.remember = True
			user.save()

		#token_expire_handler will check, if the token is expired it will generate new one
		is_expired, token = token_expire_handler(token)     # The implementation will be described further
		userd = Account.objects.get(id=token.user_id)


		return Response({
			'user': {
						'id' : userd.id,
						'username' : userd.username,
						'user_ID' : userd.user_ID,
						'email' : userd.email
					},
			'expires_in': expires_in(token),
			'token': token.key
		}, status=HTTP_200_OK)



class Logout(generics.ListCreateAPIView):
	def get(self, request, format=None):
		# simply delete the token to force a login
		# user = self.get_object()

		my_token = request.META.get('HTTP_AUTHORIZATION').split()[1]
		token = Token.objects.get(key=my_token)
		user = Account.objects.get(id=token.user_id)
		user.remember = False
		user.save()

		token.delete()

		return JsonResponse({'detail':'Logout Success'},status=status.HTTP_200_OK)



##########################################################################
##########################################################################
##########################################################################

class otp_generate(generics.GenericAPIView):
	permission_classes = (permissions.IsAuthenticated,)

	def get(self, request, format=None):

		otp = generateOTP()

		my_token = request.META.get('HTTP_AUTHORIZATION').split()[1]
		user_id = Token.objects.get(key=my_token).user_id
		user = Account.objects.get(id=user_id)
		user.otp = otp
		user.save()

		return JsonResponse({'data':{
				"otp" : str(otp),
				"user_id" : str(user_id),
			}})

def generateOTP() : 
  
    # Declare a digits variable   
    # which stores all digits  
    digits = "0123456789"
    OTP = "" 
  
   # length of password can be chaged 
   # by changing value in range 
    for i in range(6) : 
        OTP += digits[math.floor(random.random() * 10)] 
  
    return OTP 