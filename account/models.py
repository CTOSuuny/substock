import os,binascii
import random
import string
from django.db import models
from django.contrib.auth.models import AbstractUser
from binascii import hexlify
from rest_framework.authtoken.models import Token

class Account(AbstractUser):

	def _createId():
		pool = string.ascii_letters + string.digits
		return ''.join(random.choice(pool) for i in range(64))

	user_ID = models.CharField(db_index=True,max_length=100,default=_createId,unique=True)
	own_create = models.CharField(blank=True,max_length=30)

	role_ID = models.ForeignKey('RoleAccount',on_delete=models.CASCADE,null=True)
	# stock_role_ID = models.ForeignKey('stock.RoleStock',on_delete=models.CASCADE,null=True)
	stock_active_id = models.ForeignKey('stock.RoleStockOwn',on_delete=models.CASCADE,null=True)
	line_user_ID = models.CharField(blank=True,max_length=30)
	line_action = models.CharField(blank=True,max_length=30)
	token = models.CharField(blank=True,max_length=30)
	otp = models.CharField(blank=True,max_length=30)
	remember = models.BooleanField(default=False)

	def __str__(self):
		return self.username

	def as_dict(self):
		return {
			"id": self.id,
			"username" : self.username,
			"user_ID" : self.user_ID,
			"email" : self.email,
		}
		
	# def get_my_token(self):
	# 	return Token.objects.get(user=user)
	# my_token = property(get_my_token)

class RoleAccount(models.Model):

	def _createId():
		pool = string.ascii_letters + string.digits
		return ''.join(random.choice(pool) for i in range(64))

	id = models.AutoField(primary_key=True)
	role_name = models.CharField(db_index=True,max_length=30)
	role_id = models.CharField(db_index=True,max_length=100,default=_createId,unique=True)
	role_account = models.IntegerField(default=0,null=True)
	role_accountRole = models.IntegerField(default=0,null=True)
	role_stock = models.IntegerField(default=0,null=True)
	role_stockRole = models.IntegerField(default=0,null=True)
	role_item = models.IntegerField(default=0,null=True)
	role_widthdraw = models.IntegerField(default=0,null=True)
	role_transfer = models.IntegerField(default=0,null=True)
	role_adjust = models.IntegerField(default=0,null=True)
	role_announce = models.IntegerField(default=0,null=True)
	role_setting = models.IntegerField(default=0,null=True)
	date_create = models.DateTimeField(auto_now_add=True)
	date_update = models.DateTimeField(auto_now=True)

	def __str__(self):
		return self.role_name

	def as_dict(self):
		return {
			"id": self.id,
			"role_name" : self.role_name,
			"role_id" : self.role_id,
		}
