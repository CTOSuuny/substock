from rest_framework import serializers
from account.models import Account, RoleAccount
from rest_framework.authtoken.models import Token


class userSerializer(serializers.HyperlinkedModelSerializer): # new

    class Meta:
        model = Account
        fields = ( 'url', 'id', 'username', 'user_ID', 'email') # new


class userDetailSerializer(serializers.HyperlinkedModelSerializer): # new
	# token = serializers.Field(source='my_token')

	class Meta:
		model = Account
		fields = ( 'id', 'user_ID', 'username', 'email', 'first_name', 'last_name', 'line_ID') # new


class userCreateSerializer(serializers.HyperlinkedModelSerializer): # new
	# username = serializers.CharField(read_only=True)
	password = serializers.CharField(write_only=True)

	class Meta:
		model = Account
		fields = ( 'id', 'username', 'email', 'password', 'role_ID') # new

	def create(self, validated_data):
		user = super(userCreateSerializer, self).create(validated_data)
		user.set_password(validated_data['password'])
		token = Token.objects.get_or_create(user = user)
		user.save()
		return user


class RoleuserSerializer(serializers.HyperlinkedModelSerializer): # new
	
    class Meta:
        model = RoleAccount
        fields = ( 'url', 'id', 'role_id', 'role_name') # new


class RoleuserDetailSerializer(serializers.HyperlinkedModelSerializer): # new
	# token = serializers.Field(source='my_token')

	class Meta:
		model = RoleAccount
		fields = ( 'id', 'role_id', 'role_name', 'role_id', 'role_account', 'role_accountRole', 'role_stock', 'role_stockRole', 'role_item', 'role_widthdraw', 'role_transfer', 'role_adjust', 'role_announce', 'role_setting') # new

class RoleuserCreateSerializer(serializers.HyperlinkedModelSerializer): # new
	
	class Meta:
		model = RoleAccount
		fields = ( 'role_name', 'role_id', 'role_account', 'role_accountRole', 'role_stock', 'role_stockRole', 'role_item', 'role_widthdraw', 'role_transfer', 'role_adjust', 'role_announce', 'role_setting') # new


class UserSigninSerializer(serializers.Serializer):
	username = serializers.CharField(required = True)
	password = serializers.CharField(required = True)

class usersignedSerializer(serializers.Serializer):
	id = serializers.CharField(max_length=200)
	username = serializers.CharField(max_length=200)
	user_ID = serializers.CharField(max_length=200)
	email = serializers.EmailField()