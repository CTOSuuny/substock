from django.apps import AppConfig


class TransferListConfig(AppConfig):
    name = 'transfer_list'
