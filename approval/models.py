import os
from django.db import models
from binascii import hexlify

class approve_document(models.Model):

	def _createId():
		pool = string.ascii_letters + string.digits
		return ''.join(random.choice(pool) for i in range(64))

	id = models.AutoField(primary_key=True)
	approve_list_id = models.CharField(max_length=30,default=_createId,db_index=True)
	approve_document_f = models.ForeignKey('Withdraw_list.Withdraw_list', on_delete=models.CASCADE)
	approve_note = models.CharField(max_length=100)
	approve_by = models.ForeignKey('account.Account',on_delete=models.CASCADE,null=True)
	date_create = models.DateTimeField(auto_now_add=True)
	date_update = models.DateTimeField(auto_now=True)

	def __str__(self):
		return self.name
