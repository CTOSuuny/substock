import os,binascii
import random
import string
from django.db import models
from django.contrib.auth.models import AbstractUser
from binascii import hexlify
from rest_framework.authtoken.models import Token

class Setting(models.Model):

	def _createId():
		pool = string.ascii_letters + string.digits
		return ''.join(random.choice(pool) for i in range(64))

	id = models.AutoField(primary_key=True)
	name = models.CharField(db_index=True,max_length=30)
	sid = models.CharField(db_index=True,max_length=100,default=_createId,unique=True)
	timezone = models.CharField(max_length=100)
	admin_switch = models.CharField(max_length=100)

	date_create = models.DateTimeField(auto_now_add=True)
	date_update = models.DateTimeField(auto_now=True)

	def __str__(self):
		return self.role_name