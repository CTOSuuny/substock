from rest_framework import generics, permissions, renderers 
from django.http import JsonResponse
# from .models import Account
# from .serializers import userSerializer, userDetailSerializer, userCreateSerializer
from rest_framework.decorators import api_view # new
from rest_framework.response import Response
from rest_framework.authtoken.models import Token
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.permissions import IsAuthenticated
from rest_framework.authtoken.models import Token
from rest_framework.response import Response
from rest_framework.reverse import reverse # new
from rest_framework import status

from account.models import Account, RoleAccount

# @api_view(['GET']) # new
# def api_root(request, format=None):
# 	permission_classes = (permissions.IsAuthenticated,)
# 	return Response({
# 		'account': reverse('account-list', request=request, format=format),
# 		'stock': reverse('stock-list', request=request, format=format)
# 	})
SAFE_METHODS = ['GET', 'HEAD', 'OPTIONS']

class APIRoot(generics.GenericAPIView):
	permission_classes = (IsAuthenticated,)

	def get(self, request, format=None):
		return Response({
			'account': reverse('account-list', request=request, format=format),
			'role_account': reverse('role-list', request=request, format=format),
			'stock': reverse('stock-list', request=request, format=format),
			'permission': reverse('permission-list', request=request, format=format)
		})

class APIPermission(generics.GenericAPIView):
	permission_classes = (IsAuthenticated,)

	def get(self, request, format=None):

		my_token = request.META.get('HTTP_AUTHORIZATION').split()[1]
		user_id = Token.objects.get(key=my_token).user_id
		user = Account.objects.get(id=user_id)
		role = RoleAccount.objects.get(id=user.role_ID.id)

		return JsonResponse({'data':{
				"user" : user.as_dict(),
				"role_account" : permission_decode(role.role_account),
				"role_accountRole" : permission_decode(role.role_accountRole),
				"role_stock" : permission_decode(role.role_stock),
				"role_stockRole" : permission_decode(role.role_stockRole),
				"role_item" : permission_decode(role.role_item),
				"role_widthdraw" : permission_decode(role.role_widthdraw),
				"role_transfer" : permission_decode(role.role_transfer),
				"role_adjust" : permission_decode(role.role_adjust),
				"role_announce" : permission_decode(role.role_announce),
				"role_setting" : permission_decode(role.role_setting),
			}})

def permission_decode(code):

	codex = int(code)

	if codex > 7:
		rcode = 1
		codex = codex-8
	else:
		rcode = 0

	if codex > 3:
		wcode = 1
		codex = codex-4
	else:
		wcode = 0

	if codex > 1:
		ucode = 1
		codex = codex-2
	else:
		ucode = 0

	if codex > 0:
		dcode = 1
		codex = codex-1
	else:
		dcode = 0

	return {"read":rcode,
			"write":wcode,
			"update":ucode,
			"delete":dcode
			}
	
