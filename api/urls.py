from django.urls import path
from django.conf.urls import url, include
from django.contrib import admin
from rest_framework.routers import DefaultRouter
# from account.views import userList, userDetailList, UserCreateAPIView
from api import views

# schema_view = get_schema_view(title='Pastebin API') # new

urlpatterns = [
	path('', views.APIRoot.as_view() ),
	path('account/', include('account.urls')),
	path('stock/', include('stock.urls')),
	# path('detail/<int:pk>/', userDetailList.as_view(), name="account-detail"),
	# path('create/', UserCreateAPIView.as_view(), name="account-detail"),
	##################### Role #######################
	path('is_permission/', views.APIPermission.as_view(),name="permission-list"),
]

# urlpatterns = format_suffix_patterns(urlpatterns)